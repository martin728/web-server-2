const User = require('../models/User');
const Load = require('../models/Load');
const { request } = require('express');

const addLoad = async (req, res, next) => {
    try {
        if (req.user.role !== "SHIPPER") {
            res.json({
                message: 'string'
            })
        } else {
            let load = new Load({
                created_by: req.user.userId,
                name: req.body.name,
                payload: req.body.payload,
                pickup_address: req.body.pickup_address,
                delivery_address: req.body.delivery_address,
                dimensions: {
                    width: req.body.dimensions.width,
                    length: req.body.dimensions.length,
                    height: req.body.dimensions.height
                }

            })
            load.save()
                .then(load => {
                    res.json({
                        message: `Load created successfully`
                    })
                })
                .catch(err => {
                    res.json({
                        message: "string"
                    })
                })
        }
    } catch (err) {
        res.status(400);
        res.json({
            message: 'string'
        })
    }

};

const getLoads = async (req, res, next) => {
    try {
        if (req.user.role !== "SHIPPER") {
            res.status(400);
            res.json({
                message: 'string'
            })
        } else {
            let loads = await Load.find({ created_by: req.user.userId }).exec()
            res.json({
                loads
            })
        }
    } catch (err) {
        res.status(400);
        res.json({
            message: 'string'
        })
    }

};

module.exports = { addLoad, getLoads }