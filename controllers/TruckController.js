const User = require('../models/User');
const Truck = require('../models/Truck');
const { request } = require('express');


const addTruck = async (req, res, next) => {
    try {
        if (req.user.role !== "DRIVER") {
            res.json({
                message: 'string'
            })
        } else {
            let truck = new Truck({
                created_by: req.user.userId,
                type: req.body.type
            })
            truck.save()
                .then(truck => {
                    res.json({
                        message: `Truck created successfully`
                    })
                })
                .catch(err => {
                    res.json({
                        message: "string"
                    })
                })
        }
    } catch (err) {
        res.status(400);
        res.json({
            message: 'string'
        })
    }

};
const getTrucks = async (req, res, next) => {
    try {
        if (req.user.role !== "DRIVER") {
            res.status(400);
            res.json({
                message: 'string'
            })
        } else {
            let trucks = await Truck.find({ created_by: req.user.userId }).exec()
            res.json({
                trucks
            })
        }
    } catch (err) {
        res.status(400);
        res.json({
            message: 'string'
        })
    }

};
const assignTruck = async (req, res, next) => {
    let truckId = req.params.id;
    let updatedData = {
        assigned_to: req.user.userId,
    }
    try {
        if (req.user.role !== "DRIVER") {
            res.json({
                message: 'string'
            })
        } else {
            Truck.findByIdAndUpdate(truckId, { $set: updatedData })
                .then(() => {
                    res.json({
                        message: 'Truck details changed successfully'
                    })
                })
                .catch(error => {
                    res.status(500);
                    res.json({
                        message: 'string'
                    })
                })
        }
    } catch (err) {
        res.status(400);
        res.json({
            message: 'string'
        })
    }

};
const getTruckById = async (req, res, next) => {
    let truckId = req.params.id;
    try {
        if (req.user.role !== "DRIVER") {
            res.json({
                message: 'string'
            })
        } else {
            let truck = await Truck.findOne({ _id: truckId }).exec()
            res.json({
                truck
            })
        }
    } catch (err) {
        res.status(400);
        res.json({
            message: 'string'
        })
    }

};
const updateTruckById = async (req, res, next) => {
    let updatedData = {
        type: req.body.type,
    }
    let truckId = req.params.id;
    try {
        if (req.user.role !== "DRIVER") {
            res.json({
                message: 'string'
            })
        } else {
            Truck.findByIdAndUpdate(truckId, { $set: updatedData })
                .then(() => {
                    res.json({
                        message: 'Truck details changed successfully'
                    })
                })
                .catch(error => {
                    res.status(500);
                    res.json({
                        message: 'string'
                    })
                })
        }
    } catch (err) {
        res.status(400);
        res.json({
            message: 'string'
        })
    }

};
const deleteTruckById = async (req, res, next) => {
    let truckId = req.params.id;
    try {
        if (req.user.role !== "DRIVER") {
            res.json({
                message: 'string'
            })
        } else {
            Truck.findByIdAndRemove(truckId)
                .then(() => {
                    res.json({
                        message: 'Truck deleted successfully'
                    })
                })
                .catch(error => {
                    res.status(500);
                    res.json({
                        message: 'string'
                    })
                })
        }
    } catch (err) {
        res.status(400);
        res.json({
            message: 'string'
        })
    }

};

module.exports = { addTruck, getTrucks, getTruckById, updateTruckById, deleteTruckById, assignTruck }