const User = require('../models/User')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')


const authorize = async (req, res, next) => {
    try {
        let authorization = req.headers.authorization

        const token = authorization.split(' ')[1]
        const decoded = jwt.verify(token, 'verySecretValue')
        const user = await User.findById(decoded.userId).exec()

        if (!user) {
            return res.status(401).json({ message: 'Unauthorized' });
        }

        req.user = {}
        req.user.role = user.role;
        req.user.userId = decoded.userId;
        next();
    } catch (err) {
        return res.status(401).json({ message: 'Unauthorized' });
    }
}

const register = (req, res, next) => {
    bcrypt.hash(req.body.password, 10, function (err, hashedPassword) {
        if (err) {
            res.json({
                error: err
            })
        }
        let user = new User({
            role: req.body.role,
            email: req.body.email,
            password: hashedPassword,
            role: req.body.role
        })
        user.save()
            .then(user => {
                res.json({
                    message: "Profile created successfully"
                })
            })
            .catch(err => {
                res.json({
                    message: "string"
                })
            })
    })

}

const login = (req, res, next) => {
    let username = req.body.email
    let password = req.body.password

    User.findOne({ $or: [{ email: username }, { phone: username }] })
        .then(user => {
            if (user) {
                bcrypt.compare(password, user.password, function (err, result) {
                    if (err) {
                        res.json({
                            error: err
                        })
                    }
                    if (result) {
                        let jwt_token = jwt.sign({ userId: user.id }, 'verySecretValue', { expiresIn: '1h' })
                        res.json({
                            message: 'Success',
                            jwt_token
                        })
                    } else {
                        res.status(500)
                        res.json({
                            message: 'String'
                        })
                    }
                })
            } else {
                res.status(400)
                res.json({
                    message: 'String'
                })
            }
        })
}

module.exports = {
    register, login, authorize
}