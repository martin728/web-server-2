const User = require('../models/User')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs')

function checkPassword(actual, provided) {
    return bcrypt.compareSync(provided, actual);
}

const show = (req, res, next) => {
    try {
        User.findById(req.user.userId)
            .then(response => {
                res.json({
                    user: {
                        _id: response._id,
                        role: response.role,
                        email: response.email,
                        created_date: response.createdAt
                    }
                })
            })
            .catch(error => {
                res.status(500);
                res.json({
                    message: 'string'
                })
            })
    } catch (err) {
        res.status(400);
        res.json({
            message: 'string'
        })
    }
}

const update = async (req, res, next) => {

    bcrypt.hash(req.body.newPassword, 10, function (err, hashedPassword) {
        let updatedData = {
            password: hashedPassword,
        }

        User.findById(req.user.userId)
            .then(response => {
                let passMatch = checkPassword(response.password, req.body.oldPassword);
                if (passMatch) {
                    User.findByIdAndUpdate(req.user.userId, { $set: updatedData })
                        .then(() => {
                            res.json({
                                message: 'Password changed successfully'
                            })
                        })
                        .catch(error => {
                            res.status(500);
                            res.json({
                                message: 'string'
                            })
                        })
                } else {
                    res.status(400);
                    res.json({
                        message: 'string'
                    })
                }
            })
    });
}

const destroy = (req, res, next) => {
    try {

        User.findByIdAndRemove(req.user.userId)
            .then(() => {
                res.json({
                    message: 'Profile deleted successfully'
                })
            })
            .catch(error => {
                res.status(500);
                res.json({
                    message: 'string'
                })
            })
    } catch (err) {
        res.status(400);
        res.json({
            message: 'string'
        })
    }
}

module.exports = {
    show, update, destroy
}