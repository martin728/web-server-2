const express = require('express');
const router = express.Router();


const TruckController = require('../controllers/TruckController')
const AuthController = require('../controllers/AuthController')

router.post('/trucks', AuthController.authorize, TruckController.addTruck);
router.get('/trucks', AuthController.authorize, TruckController.getTrucks);
router.get('/trucks/:id', AuthController.authorize, TruckController.getTruckById);
router.put('/trucks/:id', AuthController.authorize, TruckController.updateTruckById);
router.delete('/trucks/:id', AuthController.authorize, TruckController.deleteTruckById);
router.post('/trucks/:id/assign', AuthController.authorize, TruckController.assignTruck);

module.exports = router