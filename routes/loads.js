const express = require('express');
const router = express.Router();


const LoadController = require('../controllers/LoadController')
const AuthController = require('../controllers/AuthController')

router.post('/loads', AuthController.authorize, LoadController.addLoad);
router.get('/loads', AuthController.authorize, LoadController.getLoads);
// router.get('/trucks/:id', AuthController.authorize, TruckController.getTruckById);
// router.put('/trucks/:id', AuthController.authorize, TruckController.updateTruckById);
// router.delete('/trucks/:id', AuthController.authorize, TruckController.deleteTruckById);

module.exports = router