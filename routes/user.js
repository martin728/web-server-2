const express = require('express')
const router = express.Router()

const UserController = require('../controllers/UserController')
const AuthController = require('../controllers/AuthController')

router.get('/users/me', AuthController.authorize, UserController.show)
router.delete('/users/me', AuthController.authorize, UserController.destroy)
router.patch('/users/me/password', AuthController.authorize, UserController.update)

module.exports = router