require('dotenv').config();
const express = require('express')
const mongoose = require('mongoose')
const morgan = require('morgan')
const bodyParser = require('body-parser')

const UserRoute = require('./routes/user')
const AuthRoute = require('./routes/auth')
const TruckRoute = require('./routes/track')
const LoadRoute = require('./routes/loads')
const cors = require('./middleware/cors')

mongoose.connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

const db = mongoose.connection
db.on('error', (err) => {
    console.log(err)
})
db.once('open', () => {
    console.log('DB Connection established')
})
const app = express()

app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`)
})
app.use(cors);
app.use('/api/auth', AuthRoute)
app.use('/api', UserRoute)
app.use('/api', TruckRoute)
app.use('/api', LoadRoute)